#!/bin/bash

for f in `ls ./BMFonts/$1/*.json`
do
  echo "Processing $f file..."
  ./bmfb.py "$f"
done
